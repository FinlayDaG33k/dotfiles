# Install all software
sudo pamac install gitahead-bin
sudo pamac install rambox-bin
sudo pamac install stacer-bin
sudo pacman -Sy --no-confirm \
  rofi \
  plank \
  python-pip \
  docker \
  docker-compose \
  thunderbird \
  firefox \
  yubioath-desktop \ 
  pam-u2f \
  kleopatra \
  iio-sensor-proxy \
  screenrotator-git
sudo pip install powerline-status

# Setup Plank
# TODO: Add it to the startup
cp -r .local/share/applications ~/.local/share
cp -r .local/share/plank/themes ~/.local/share/plank
cp -r .config/plank ~/.config

# Setup Rofi
# TODO: Replace whiskermenu with rofi
cp -r .config/rofi ~/.config

# Setup Docker
sudo usermod -aG docker $(whoami)
echo "" >> ~/.xprofile
echo "# Docker things" >> ~/.xprofile
echo "export DOCKER_HOST=unix:///var/run/docker.sock" >> ~/.xprofile
sudo systemctl enable docker

# Setup XFCE4
xfconf-query -c xfwm4 -p /general/show_dock_shadow -s false
xfconf-query -c xfwm4 -p /general/show_frame_shadow -s false
xfconf-query -c xfwm4 -p /general/workspace_count -s 1
cp -r .config/xfce4/xfconf/xfce-perchannel-xml ~/.config/xfce4/xfconf/xfce-perchannel-xml

# Setup Powerline-status
echo "" >> ~/.bashrc
echo "# Powerline" >> ~/.bashrc
echo "powerline-daemon -q" >> ~/.bashrc
echo "POWERLINE_BASH_CONTINUATION=1" >> ~/.bashrc
echo "POWERLINE_BASH_SELECT=1" >> ~/.bashrc
echo ". /usr/lib/python3.8/site-packages/powerline/bindings/bash/powerline.sh" >> ~/.bashrc

# Setup SSH for use with Yubikey
mkdir ~/.gnupg
touch ~/.gnupg/gpg-agent.conf
echo "enable-ssh-support" >> ~/.gnupg/gpg-agent.conf
echo ""  >> ~/.xprofile
echo "# Set the gpg-agent for SSH" >> ~/.xprofile
echi "export GPG_TTY=$(tty)" >> ~/.xprofile
echo "gpg-agent --daemon --enable-ssh-support &" >> ~/.xprofile
echo "gpg-connect-agent /bye;" >> ~/.xprofile
echo "export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket);" >> ~/.xprofile
xfconf-query -c xfce4-session -p /startup/ssh-agent/enabled -n -t bool -s false

# Setup login with yubikey
# TODO: Fix issue where login is not working
mkdir ~/.config/Yubico
touch ~/.config/Yubico/u2f_keys
read -p "Insert Yubikey then press enter to continue"
echo "Please press the pad on Yubikey"
echo $(pamu2fcfg -u$(whoami)) >> ~/.config/Yubico/u2f_keys
sudo sed -i '2 i\\n# Yubikey login\nauth      sufficient pam_u2f.so debug' /etc/pam.d/system-local-login

# Setup login with fingerprint
# TODO: Wait for fprintd to support my fingerprint scanner (ID: 27c6:538c)
# TODO: Add fprintd to the list of dependencies
# sudo sed -i '4 i\\n# Fingerprint login\nauth      sufficient pam_fprintd.so' /etc/pam.d/system-local-login

# Copy the git config
cp .gitconfig ~/.gitconfig

# Enable scrolling with the touchscreen in Firefox
echo "" >> ~/.xprofile
echo "# Enable touchscreen support on Firefox" >> ~/.xprofile
echo "export MOZ_USE_XINPUT2=1" >> ~/.xprofile 

# Ask the user to reboot
echo "Please reboot when it's convenient so we can make sure everything is properly reloaded"
echo "After this, there are some manual things you'll have to do on your own:"
echo "- Login into all your accounts in Firefox"
echo "- Login into all your accounts in Thunderbirb"
echo "- Login into all your accounts in Rambox"
echo "- Setup Kleopatra"
