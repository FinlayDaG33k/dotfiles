#!/bin/bash

# Define my colors
blue=$(tput setaf 33)
white=$(tput setaf 15)

# Set some additional variables
gpg_key="0x738EE8DBC43F9861"
username=$(whoami)
hostname=$(hostname)
packages=$(pacman -Q | wc -l)

# Get the OS info
. /etc/os-release
. /etc/lsb-release
distro=$ID
version=$DISTRIB_RELEASE
codename=$DISTRIB_CODENAME

# Get some HW specs
cpu_model=$(lscpu | sed -nr '/Model name/ s/.*:\s*(.*) CPU @ .*/\1/p')
ram_size=$(free -h | awk '/Mem:/ { print $2 }')

# Display
echo ""
echo -e "${blue}+-------+${white}-------\\ ${blue}+-----------+"
echo -e "${blue}|       |${white}        \\${blue}|           |      ${white}${username}${blue}@${white}${hostname}"
echo -e "${blue}|   +---+${white}  +---+  ${blue}|   +---+   |      -------------------"
echo -e "${blue}|   |   ${white}|  |   |  ${blue}|   |   +---+      ${blue}GPG:      ${white}${gpg_key}"
echo -e "${blue}|   +---+${white}  |   |  ${blue}|   |              ${blue}OS:       ${white}${distro^} ${version} (\"${codename}\")"
echo -e "${blue}|       |${white}  |   |  ${blue}|   |+------+      ${blue}CPU:      ${white}${cpu_model}"
echo -e "${blue}|   +---+${white}  |   |  ${blue}|   |+--+   |      ${blue}RAM:      ${white}${ram_size}B"
echo -e "${blue}|   |   ${white}|  |   |  ${blue}|   |   |   |      ${blue}Packages: ${white}${packages}"
echo -e "${blue}|   |   ${white}|  +---+  ${blue}|   +---+   |"
echo -e "${blue}|   |   ${white}|        /${blue}|           |"
echo -e "${blue}+---+   ${white}+-------/ ${blue}+-----------+"
echo ""
